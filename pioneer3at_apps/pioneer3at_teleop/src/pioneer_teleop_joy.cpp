#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>


class TeleopPioneer
{
public:
  TeleopPioneer();

private:
  void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);
  
  ros::NodeHandle nh_;

  int linear_, angular_;
  double l_scale_, a_scale_;
  ros::Publisher vel_pub_;
  ros::Subscriber joy_sub_;
  
};


TeleopPioneer::TeleopPioneer():
  linear_(1),
  angular_(2)
{

  nh_.param("axis_linear", linear_, linear_);
  nh_.param("axis_angular", angular_, angular_);
  nh_.param("scale_angular", a_scale_, a_scale_);
  nh_.param("scale_linear", l_scale_, l_scale_);


  vel_pub_ = nh_.advertise<geometry_msgs::Twist>("cmd_vel", 10);


  joy_sub_ = nh_.subscribe<sensor_msgs::Joy>("joy", 10, &TeleopPioneer::joyCallback, this);

}

void TeleopPioneer::joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  geometry_msgs::Twist vel;
  vel.angular.x = 0;
  vel.angular.y = 0;
  vel.angular.z = a_scale_*joy->axes.at(0);
  vel.linear.x = l_scale_*joy->axes.at(1);
  vel.linear.y = 0;
  vel.linear.z = 0;
  vel_pub_.publish(vel);
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "teleop_pioneer");
  TeleopPioneer teleop_pioneer;

  ros::spin();
}