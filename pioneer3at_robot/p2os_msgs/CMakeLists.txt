cmake_minimum_required(VERSION 2.8.3)
project(p2os_msgs)

find_package(catkin REQUIRED COMPONENTS
  message_generation
  std_msgs
  geometry_msgs
)

add_message_files(
  FILES
  TCMState.msg
  AIO.msg
  ArmState.msg
  BatteryState.msg
  DIO.msg
  GripperState.msg
  GripState.msg
  LiftState.msg
  MotorState.msg
  PTZState.msg
  SonarArray.msg
  SonarState.msg
)

generate_messages(DEPENDENCIES std_msgs geometry_msgs)

catkin_package(
	CATKIN_DEPENDS message_runtime std_msgs geometry_msgs
)

include_directories(
  ${catkin_INCLUDE_DIRS}
)