import roslib;roslib.load_manifest('p2os_rqt_dashboard')
import rospy

from python_qt_binding.QtCore import QSize

from rqt_robot_dashboard.widgets import MenuDashWidget

class P2osMotors(MenuDashWidget):
    """
    Dashboard widget to display the P2OS Motors MotorState and allow interaction
    """
    def __init__(self, start_motors_callback, stop_motors_callback):
        ok_icon = ['bg-green.svg', 'ic-motors.svg']
        err_icon = ['bg-red.svg', 'ic-motors.svg', 'ol-err-badge.svg']

        icons = [ok_icon, err_icon]

        super(P2osMotors, self).__init__('Motors', icons, icons)
        self.setToolTip('Motors State')
        self.add_action('Start Motors', start_motors_callback)
        self.add_action('Stop Motors', stop_motors_callback)
        self.set_err()
        self.setFixedSize(self._icons[0].actualSize(QSize(50, 30)))

    def set_ok(self):
    	self.update_state(0)

    def set_err(self):
    	self.update_state(1)


