import roslib;roslib.load_manifest('p2os_rqt_dashboard')
import rospy

from python_qt_binding.QtCore import QSize

from rqt_robot_dashboard.widgets import MenuDashWidget

class P2osControllers(MenuDashWidget):
    """
    Dashboard widget to display the P2OS Controllers and allow interaction
    """
    def __init__(self, load_traj_callback, unload_traj_callback, start_traj_callback, stop_traj_callback):
        traj_start_icon = ['bg-green.svg', 'ic-controller.svg']
        traj_load_icon = ['bg-orange.svg', 'ic-controller.svg']
        traj_unload_icon = ['bg-red.svg', 'ic-controller.svg']

        icons = [traj_start_icon, traj_load_icon, traj_unload_icon]

        super(P2osControllers, self).__init__('Arm Controllers', icons, icons, icon_paths=[['p2os_rqt_dashboard', 'images']])
        self.setToolTip('Controllers State')
        self.add_action('Load Controllers', load_traj_callback)
        self.add_action('Unload Controllers', unload_traj_callback)
        self.add_action('Start Controllers', start_traj_callback)
        self.add_action('Stop Controllers', stop_traj_callback)
        self.set_traj_unload()
        self.setFixedSize(self._icons[0].actualSize(QSize(50, 30)))

    def set_traj_start(self):
    	self.update_state(0)

    def set_traj_load(self):
        self.update_state(1)

    def set_traj_unload(self):
        self.update_state(2)

    def get_traj_status(self):
        return self.state