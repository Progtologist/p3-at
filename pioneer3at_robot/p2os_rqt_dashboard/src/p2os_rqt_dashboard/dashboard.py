import roslib;roslib.load_manifest('p2os_rqt_dashboard')
import rospy

import diagnostic_msgs
import p2os_msgs.msg

from rqt_robot_dashboard.dashboard import Dashboard
from rqt_robot_dashboard.widgets import MonitorDashWidget, ConsoleDashWidget, MenuDashWidget, BatteryDashWidget, IconToolButton, NavViewDashWidget
from QtGui import QMessageBox, QAction
from python_qt_binding.QtCore import QSize
from p2os_msgs.msg import MotorState, SonarState, ArmState, TCMState
from controller_manager_msgs.srv import LoadController, UnloadController, SwitchController

from .battery import P2osBattery
from .motors import P2osMotors
from .sensors import P2osSensors
from .arm import P2osArm
from .controllers import P2osControllers

import rospkg
import os.path

rp = rospkg.RosPack()

image_path = image_path = os.path.join(rp.get_path('p2os_rqt_dashboard'), 'images')

class P2OSDashboard(Dashboard):
    def setup(self, context):

        # Topics Publishers and Subscribers
        self.motor_pub = rospy.Publisher('/cmd_motor_state', MotorState)
        self.motor_sub = rospy.Subscriber('/cmd_motor_state', MotorState, self.motor_change_callback)
        self.sonar_pub = rospy.Publisher('/sonar_control', SonarState)
        self.sonar_sub = rospy.Subscriber('/sonar_control', SonarState, self.sonar_change_callback)
        self.tcm_pub   = rospy.Publisher('/tcm_control', TCMState)
        self.tcm_sub   = rospy.Subscriber('/tcm_control', TCMState, self.tcm_change_callback)
        self.arm_pub   = rospy.Publisher('/arm_control', ArmState)
        self.arm_sub   = rospy.Subscriber('/arm_control', ArmState, self.arm_change_callback)
        rospy.wait_for_service('/controller_manager/load_controller')
        self.load_controller_service = rospy.ServiceProxy('/controller_manager/load_controller', LoadController)
        self.unload_controller_service = rospy.ServiceProxy('/controller_manager/unload_controller', UnloadController)
        self.switch_controller_service = rospy.ServiceProxy('/controller_manager/switch_controller', SwitchController)

        self.message = None
        self.sonar_current_state = 0
        self.tcm_current_state   = 0
        self._dashboard_message = None
        self._last_dashboard_message_time = 0.0

        self.p2os_bat = P2osBattery("Battery")
        self.p2os_mot = P2osMotors(self.on_start_motors,self.on_stop_motors)
        self.p2os_sen = P2osSensors(self.on_start_sonar,self.on_stop_sonar,self.on_start_tcm,self.on_stop_tcm)
        self.p2os_arm = P2osArm(self.on_start_arm,self.on_stop_arm)
        self.p2os_controllers = P2osControllers(self.on_load_traj, self.on_unload_traj, self.on_start_traj, self.on_stop_traj)
        self.console = ConsoleDashWidget(self.context)
        self.monitor = MonitorDashWidget(self.context)
        self.nav = NavViewDashWidget(self.context)

        # This is what gets dashboard_callback going eagerly
        self._dashboard_agg_sub = rospy.Subscriber('diagnostics_agg', diagnostic_msgs.msg.DiagnosticArray, self.dashboard_callback)

    def get_widgets(self):
        return [[self.monitor, self.console]
        ,[self.p2os_mot, self.p2os_sen]
        ,[self.p2os_arm, self.p2os_controllers]
        ,[self.p2os_bat]
        ,[self.nav]]

    def dashboard_callback(self, msg):
        self._dashboard_message = msg
        self._last_dashboard_message_time = rospy.get_time()
  
        battery_status = {}  # Used to store P2osBattery status info
        for status in msg.status:
            if status.name == "/Sensors/p2os/p2os_driver: Battery Voltage":
                for value in status.values:
                    battery_status[value.key]=value.value

        # If battery diagnostics were found, calculate percentages and stuff  
        if (battery_status):
            self.p2os_bat.set_power_state(battery_status)

    def on_start_motors(self):
        self.motor_pub.publish(MotorState(1))

    def on_stop_motors(self):
        self.motor_pub.publish(MotorState(0))

    def on_start_sonar(self):
        self.sonar_pub.publish(SonarState(1))

    def on_stop_sonar(self):
        self.sonar_pub.publish(SonarState(0))

    def on_start_tcm(self):
        self.tcm_pub.publish(TCMState(1))

    def on_stop_tcm(self):
        self.tcm_pub.publish(TCMState(0))

    def on_start_arm(self):
        self.arm_pub.publish(ArmState(1))

    def on_stop_arm(self):
        self.arm_pub.publish(ArmState(0))

    def on_load_traj(self):
        self.load_controller_service('PArm_controller')
        self.load_controller_service('Gripper_controller')
        self.load_controller_service('joint_state_controller')
        self.p2os_controllers.set_traj_load()

    def on_unload_traj(self):
        self.unload_controller_service('PArm_controller')
        self.unload_controller_service('Gripper_controller')
        self.unload_controller_service('joint_state_controller')
        self.p2os_controllers.set_traj_unload()

    def on_start_traj(self):
        if (self.p2os_controllers.get_traj_status() == 1):
            self.switch_controller_service(['joint_state_controller','PArm_controller','Gripper_controller'],[],2)
            self.p2os_controllers.set_traj_start()

    def on_stop_traj(self):
        if (self.p2os_controllers.get_traj_status() == 0):
            self.switch_controller_service([],['joint_state_controller','PArm_controller','Gripper_controller'],2)
            self.p2os_controllers.set_traj_load()

    def motor_change_callback(self, msg):
        if (msg.state):
            self.p2os_mot.set_ok()
        else:
            self.p2os_mot.set_err()

    def sonar_change_callback(self, msg):
        if (msg.array_power):
            self.sonar_current_state = 1
        else:
            self.sonar_current_state = 0
        self.p2os_sen.set_state(self.sonar_current_state,self.tcm_current_state)

    def tcm_change_callback(self,msg):
        if (msg.tcm_power):
            self.tcm_current_state = 1
        else:
            self.tcm_current_state = 0
        self.p2os_sen.set_state(self.sonar_current_state,self.tcm_current_state)

    def arm_change_callback(self, msg):
        if (msg.arm_power):
            self.p2os_arm.set_ok()
        else:
            self.p2os_arm.set_err()

    def shutdown_dashboard(self):
        self._dashboard_agg_sub.unregister()

    def save_settings(self, plugin_settings, instance_settings):
        self.console.save_settings(plugin_settings, instance_settings)
        self.monitor.save_settings(plugin_settings, instance_settings)

    def restore_settings(self, plugin_settings, instance_settings):
        self.console.restore_settings(plugin_settings, instance_settings)
        self.monitor.restore_settings(plugin_settings, instance_settings)