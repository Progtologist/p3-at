import roslib;roslib.load_manifest('p2os_rqt_dashboard')
import rospy

from python_qt_binding.QtCore import QSize

from rqt_robot_dashboard.widgets import MenuDashWidget

class P2osSensors(MenuDashWidget):
    """
    Dashboard widget to display the P2OS SonarState and allow interaction
    """
    def __init__(self, start_sonar_callback, stop_sonar_callback, start_tcm_callback, stop_tcm_callback):
        all_icon = ['bg-green.svg', 'ic-sensor.svg']
        one_icon = ['bg-yellow.svg', 'ic-sensor.svg']
        err_icon = ['bg-red.svg', 'ic-sensor.svg', 'ol-err-badge.svg']

        icons = [all_icon, one_icon, err_icon]

        super(P2osSensors, self).__init__('Sensors', icons, icons)
        self.setToolTip('Sonar State')
        self.add_action('Start Sonar', start_sonar_callback)
        self.add_action('Stop Sonar', stop_sonar_callback)
        self.add_action('Start TCM2', start_tcm_callback)
        self.add_action('Stop TCM2', stop_tcm_callback)
        self.set_err()
        self.setFixedSize(self._icons[0].actualSize(QSize(50, 30)))

    def set_all(self):
    	self.update_state(0)

    def set_one(self):
        self.update_state(1)

    def set_err(self):
    	self.update_state(2)

    def set_state(self,sonar,tcm):
        if (sonar and tcm):
            self.set_all()
        elif ( (sonar and ~tcm) or (~sonar and tcm) ):
            self.set_one()
        else:
            self.set_err()
