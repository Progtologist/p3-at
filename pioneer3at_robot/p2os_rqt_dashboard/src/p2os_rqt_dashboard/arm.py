import roslib;roslib.load_manifest('p2os_rqt_dashboard')
import rospy

from python_qt_binding.QtCore import QSize

from rqt_robot_dashboard.widgets import MenuDashWidget

class P2osArm(MenuDashWidget):
    """
    Dashboard widget to display the P2OS ArmState and allow interaction
    """
    def __init__(self, start_arm_callback, stop_arm_callback):
        ok_icon = ['bg-green.svg', 'ic-arm.svg']
        err_icon = ['bg-red.svg', 'ic-arm.svg', 'ol-err-badge.svg']

        icons = [ok_icon, err_icon]

        super(P2osArm, self).__init__('Arm', icons, icons, icon_paths=[['p2os_rqt_dashboard', 'images']])
        self.setToolTip('Arm State')
        self.add_action('Start Arm', start_arm_callback)
        self.add_action('Stop Arm', stop_arm_callback)
        self.set_err()
        self.setFixedSize(self._icons[0].actualSize(QSize(50, 30)))

    def set_ok(self):
    	self.update_state(0)

    def set_err(self):
    	self.update_state(1)


