import roslib;roslib.load_manifest('p2os_rqt_dashboard')
import rospy

from rqt_robot_dashboard.widgets import BatteryDashWidget

def non_zero(value): 
   if value < 0.00001 and value > -0.00001: 
      return 0.00001 
   return value

class P2osBattery(BatteryDashWidget):
    def __init__(self, name='battery'):
        icons = []
        charge_icons = []
        icons.append(['ic-battery-0.svg'])
        icons.append(['ic-battery-20.svg'])
        icons.append(['ic-battery-40.svg'])
        icons.append(['ic-battery-60-green.svg'])
        icons.append(['ic-battery-80-green.svg'])
        icons.append(['ic-battery-100-green.svg'])
        charge_icons.append(['ic-battery-charge-0.svg'])
        charge_icons.append(['ic-battery-charge-20.svg'])
        charge_icons.append(['ic-battery-charge-40.svg'])
        charge_icons.append(['ic-battery-charge-60-green.svg'])
        charge_icons.append(['ic-battery-charge-80-green.svg'])
        charge_icons.append(['ic-battery-charge-100-green.svg'])
        super(P2osBattery, self).__init__(name=name, icons=icons, charge_icons=charge_icons)

        self._pct = 0.0
        self._vlt = 0.0

    def set_power_state(self, msg):
        last_pct = self._pct
        last_vlt = self._vlt
        self._pct = (float(msg['Voltage']) - 11.3) / 2.2
        if self._pct > 1.0:
            self._pct = 1.0
        
        if self._pct < 0.0:
            self._pct = 0.0
        self._vlt = float(msg['Voltage'])
        
        self.update_perc(self._pct*100)
        self.update_time(self._pct*100)

