cmake_minimum_required(VERSION 2.8.3)
project(p2os_driver)

find_package(catkin REQUIRED COMPONENTS roscpp geometry_msgs tf std_msgs hardware_interface controller_manager p2os_msgs)

###################################################
## Declare things to be passed to other projects ##
###################################################

## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
   INCLUDE_DIRS include
#  LIBRARIES p2os_driver
   CATKIN_DEPENDS roscpp geometry_msgs tf std_msgs hardware_interface controller_manager p2os_msgs
#  DEPENDS system_lib
)

###########
## Build ##
###########

## Specify additional locations of header files
include_directories(include
  ${catkin_INCLUDE_DIRS}
)

## Declare a cpp executable
add_executable(p2os_driver  src/p2osnode.cc
                            src/p2os.cc         include/p2os.h
                            src/kinecalc.cc     include/kinecalc.h
                            src/packet.cc       include/packet.h
                            src/robot_params.cc include/robot_params.h
                            src/sip.cc          include/sip.h
                            src/p2os_ptz.cpp    include/p2os_ptz.h)
target_link_libraries(p2os_driver ${catkin_LIBRARIES})
add_dependencies(p2os_driver p2os_msgs_gencpp)

#############
## Install ##
#############

## Mark executables and/or libraries for installation
install(TARGETS p2os_driver
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

## Mark other files for installation (e.g. launch and bag files, etc.)
 install(DIRECTORY 
   launch
   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
 )



