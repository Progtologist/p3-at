/*
 *  P2OS for ROS
 *  Copyright (C) 2009
 *     David Feil-Seifer, Brian Gerkey, Kasper Stoy,
 *      Richard Vaughan, & Andrew Howard
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef _P2OSDEVICE_H
#define _P2OSDEVICE_H

#include <pthread.h>
#include <sys/time.h>
#include <iostream>
#include <string.h>

#include "packet.h"
#include "robot_params.h"

#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Twist.h"
#include <std_srvs/Empty.h>
#include <sensor_msgs/Range.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/MagneticField.h>
#include <sensor_msgs/Temperature.h>
#include <tf/transform_broadcaster.h>
#include <p2os_msgs/BatteryState.h>
#include <p2os_msgs/MotorState.h>
#include <p2os_msgs/GripperState.h>
#include <p2os_msgs/SonarState.h>
#include <p2os_msgs/SonarArray.h>
#include <p2os_msgs/DIO.h>
#include <p2os_msgs/AIO.h>
#include <p2os_msgs/ArmState.h>
#include <p2os_msgs/TCMState.h>

#include <diagnostic_updater/publisher.h>
#include <diagnostic_updater/diagnostic_updater.h>

#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>

#include <controller_manager/controller_manager.h>

//#include <joint_limits_interface/joint_limits.h>
//#include <joint_limits_interface/joint_limits_urdf.h>
//#include <joint_limits_interface/joint_limits_rosparam.h>

typedef struct ros_p2os_data
{
    nav_msgs::Odometry position;
    p2os_msgs::BatteryState batt;
    p2os_msgs::MotorState motors;
    p2os_msgs::GripperState gripper;
    p2os_msgs::SonarArray sonar;
    p2os_msgs::DIO dio;
    p2os_msgs::AIO aio;
    geometry_msgs::TransformStamped odom_trans;
    sensor_msgs::Imu compass;
    sensor_msgs::MagneticField magnetic;
    sensor_msgs::Temperature temp;
} ros_p2os_data_t;

// this is here because we need the above typedef's before including it.
#include "sip.h"
#include "kinecalc.h"

#include "p2os_ptz.h"

class SIP;

// Forward declaration of the KineCalc_Base class declared in kinecalc_base.h
//class KineCalc;


class P2OSNode : public hardware_interface::RobotHW
{
public:
    P2OSNode( ros::NodeHandle n );
    virtual ~P2OSNode();

    geometry_msgs::Twist      cmdvel_;
    p2os_msgs::MotorState   cmdmotor_state_;
    p2os_msgs::GripperState gripper_state_;
    ros_p2os_data_t           p2os_data;
    
    int Setup();
    int SetupTCP();
    int Shutdown();
    int SendReceive(P2OSPacket* pkt, bool publish_data = true );
    void updateDiagnostics();
    void ResetRawPositions();
    void SendPulse (void);
    void check_and_set_vel();
    void check_and_set_motor_state();
    void check_and_set_gripper_state();

    double get_pulse() {return pulse;}
    bool get_psos_use_tcp() {return psos_use_tcp;}
    double get_frequency() {return frequency;}
    void check_and_set_arm_state(ros::Time time, ros::Duration period, controller_manager::ControllerManager &cm);

protected:
    // Callbacks
    void cmdvel_cb( const geometry_msgs::TwistConstPtr &);
    void cmdmotor_state( const p2os_msgs::MotorStateConstPtr &);
    void gripperCallback(const p2os_msgs::GripperStateConstPtr &msg);
    void sonar_cb(const p2os_msgs::SonarStateConstPtr &msg);
    void arm_cb(const p2os_msgs::ArmStateConstPtr &msg);
    void tcm_cb(const p2os_msgs::TCMStateConstPtr &msg);

    // diagnostic messages
    void check_voltage( diagnostic_updater::DiagnosticStatusWrapper &stat );
    void check_stall( diagnostic_updater::DiagnosticStatusWrapper &stat );
    void registerMotors();

    void ToggleSonarPower(unsigned char val);
    void ToggleMotorPower(unsigned char val);
    void ToggleTCMPower(unsigned char val);
    void ToggleArmPower(bool val);
    void StandardSIPPutData(ros::Time ts);
    void TCMSIPPutData(ros::Time ts);
    void arm_stop();
    void arm_start();
    void arm_initialize();
    void read_arm_state();
    void write_arm_state();

    void setFileCloseOnExec(int fd, bool closeOnExec = true);
    ros::NodeHandle n;
    ros::NodeHandle nh_private;
    bool psos_use_tcp;
    bool use_arm_, arm_started_, use_tcm_, tcm_started_;
    bool arm_initialized_;

    inline double TicksToDegrees (int joint, unsigned char ticks);
    inline unsigned char DegreesToTicks (int joint, double degrees);
    inline double TicksToRadians (int joint, unsigned char ticks);
    inline unsigned char RadiansToTicks (int joint, double rads);
    inline double RadsPerSectoSecsPerTick (int joint, double speed);
    inline double SecsPerTicktoRadsPerSec (int joint, double secs);

    diagnostic_updater::Updater diagnostic_;
    diagnostic_updater::DiagnosedPublisher<p2os_msgs::BatteryState> batt_pub_;

    ros::Publisher  pose_pub_,
    mstate_pub_,
    grip_state_pub_,
    ptz_state_pub_,
    sonar_pub_,
    aio_pub_,
    dio_pub_,
    imu_pub_,
    mag_pub_,
    temp_pub_;


    ros::Subscriber arm_sub_,
    cmdvel_sub_,
    cmdmstate_sub_,
    gripper_sub_,
    sonar_sub_,
    tcm_sub_,
    ptz_cmd_sub_;

    tf::TransformBroadcaster odom_broadcaster;
    ros::Time veltime;

    hardware_interface::JointStateInterface jnt_state_interface;
    hardware_interface::PositionJointInterface jnt_pos_interface;
    hardware_interface::JointStateHandle state_handler;
    hardware_interface::JointHandle pos_handler;
    std::vector<double> arm_cmd;
    std::vector<double> arm_pos;
    std::vector<double> arm_vel;
    std::vector<double> arm_eff;
    std::vector<double> mot_pos;
    std::vector<double> mot_vel;
    std::vector<double> mot_eff;

    SIP* sippacket;
    std::string psos_serial_port;
    std::string psos_tcp_host;
    int         psos_fd;
    int         psos_tcp_port;
    bool        vel_dirty, motor_dirty;
    bool        gripper_dirty_;
    int         param_idx;
    double      frequency;
    int         pulse; // Rate of pulses
    // PID settings
    int rot_kp, rot_kv, rot_ki, trans_kp, trans_kv, trans_ki;

    int bumpstall;
    int joystick;
    int direct_wheel_vel_control;
    int radio_modemp;
    int NumberOfJoints;
    int motor_max_speed;
    int motor_max_turnspeed;
    short motor_max_trans_accel, motor_max_trans_decel;
    short motor_max_rot_accel, motor_max_rot_decel;
    double lastPulseTime; // Last time of sending a pulse or command to the robot
    bool use_sonar_, sonar_started_;
    
    P2OSPtz ptz_;
};

#endif
