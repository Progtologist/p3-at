cmake_minimum_required(VERSION 2.8.3)
project(pioneer3at_gazebo)
find_package(catkin REQUIRED COMPONENTS)

find_package(gazebo REQUIRED)

include_directories(include ${catkin_INCLUDE_DIRS} ${GAZEBO_INCLUDE_DIRS} ${SDFormat_INCLUDE_DIRS})
catkin_package(
    DEPENDS
    CATKIN_DEPENDS
    INCLUDE_DIRS 
    LIBRARIES 
)